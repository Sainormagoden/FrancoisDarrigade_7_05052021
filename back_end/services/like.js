const db = require("../models");
const Post = db.posts;

/**
 * Add like on post.
 * @param {*} id_post ID of post to add a like.
 * @param {*} id_user ID of user who add a like.
 * @returns Return a simple text if success or a error.
 */
exports.addPostLike = (id_post, id_user) => {
    return Post.findByPk(id_post)
        .then((post) => {
            return post.addUser(id_user)
                .then (() => {
                    return 'Liked'
                })
                .catch((err) => {
                    throw err.message
                })
        })
        .catch((err) => {
            throw err.message
        })
};

/**
 * Remove like on post.
 * @param {*} id_post ID of post to remove a like.
 * @param {*} id_user ID of user who remove a like.
 * @returns Return a simple text if success or a error.
 */
exports.removePostLike = (id_post, id_user) => {
    return Post.findByPk(id_post)
    .then((post) => {
        return post.removeUser(id_user)
            .then (() => {
                return 'Unliked'
            })
            .catch((err) => {
                throw err.message
            })
    })
    .catch((err) => {
        throw err.message
    })
};