/**
 * Check if post data is correct
 */
module.exports = (req, res, next) => {
    try {
        // Check with forum.
        switch(Number(req.body.id_forum)) {
            case 1 : {
                req.body.image = null;
                break
            }
            case 2 : {
                req.body.text = null;
                break
            }
            case 3 : {
                if (req.body.text && req.body.image) {
                    break
                } else {
                    console.log(req.body)
                    return res.status(400).json({succes: false, message: "Text and image is required for this forum"});
                }
            }
            default : {
                res.status(403).json({ error: 'Invalid request!' });
            }
        }
        
        // Validate request.
        if ((!req.body.text && !req.body.image) || !req.body.id_forum || !req.body.id_user) {
            return res.status(400).json({succes: false, message: "Invalid parameters provided !"});
        } else {
            next();
        };
    }
    catch {
        res.status(403).json({ error: 'Invalid request!' });
    }
};