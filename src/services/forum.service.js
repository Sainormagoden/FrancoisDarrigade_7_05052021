import axios from 'axios';
import authHeader from './auth-header';
import config from '../config/config';

const API_URL = config.API_URL+'forum/';

class PostService {
    getAll() {
      return axios.get(API_URL, { headers: authHeader() });
    }
}
  
export default new PostService();