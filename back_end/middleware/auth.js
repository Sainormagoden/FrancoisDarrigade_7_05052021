const jwt = require('jsonwebtoken');
const config = require('../config.js');
const db = require("../models");
const User = db.users;
const Role = db.roles;

/**
 * Check authorization with a token
 */
module.exports = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, config.JWT_TOKEN);
        const userId = decodedToken.userId;
        const userRole = decodedToken.userRole;
        if(userId && userRole) {
            const roleInBDD = await User.findByPk(userId,{ include: [{ model: Role, as: 'role'}] } ).then((data) => {return data.role.role_name});
            if (roleInBDD === userRole) {
            req.params.rights = {
                userId,
                userRole
            }
            next();
            } else { 
                res.status(401).json({ error: 'Invalid data in Token' });
            }
        } else {
            res.status(401).json({ error: 'Invalid Token' });
        }
    } catch (err) {
        res.status(401).json({ error: 'Invalid request!' });
    }
};