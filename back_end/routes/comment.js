const express = require('express');
const router = express.Router();
const commentCtrl = require('../controllers/comment');
const auth = require('../middleware/auth');
const verifData = require('../middleware/verifCommentData');
const verifRightUser = require('../middleware/verifRightUser');

// Create a comment.
router.post('/:id_post', auth, verifData, commentCtrl.create);

// Modify a comment.
router.put('/:id', auth, verifRightUser.check('Comment'), verifData, commentCtrl.update);

// Delete a comment.
router.delete('/:id', auth, verifRightUser.check('Comment'), commentCtrl.delete);

module.exports = router;