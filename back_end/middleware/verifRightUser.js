const db = require("../models");
const Post = db.posts;
const Comment = db.comments;

/**
 * Check authorization with role and id.
 * @param {string} model The model to check.
 */
 exports.check = function async (model) {

    /**
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    return async (req, res, next) => {

        // Check with model.
        let id_model;
        switch(model) {
            case 'Post' : {
                id_model = await Post.findByPk(req.params.id).then((data) => {return data.id_user});
                break
            }
            case 'Comment' : {
                id_model = await Comment.findByPk(req.params.id).then((data) => {return data.id_user});
                break
            }
            case 'User' : {
                id_model = req.params.id;
                break                   
            }
            default : {
                return res.status(500).json({ error: 'Error in configuration of middleware' });
            }
        }
        if (req.params.rights.userId == id_model || req.params.rights.userRole === 'Admin') {
            next();
        } else {
            return res.status(401).json({ error: 'Unauthorized' });
        }
    };
};