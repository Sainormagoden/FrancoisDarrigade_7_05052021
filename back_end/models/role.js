module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define("role", {
        role_name: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        }
    });
    return Role;
};