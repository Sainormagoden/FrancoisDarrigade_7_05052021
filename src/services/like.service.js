import axios from 'axios';
import authHeader from './auth-header';
import config from '../config/config';

const API_URL = config.API_URL+'like/';

class LikeService {

  addPostLike(id_post, id_user) {
    return axios.post(API_URL + 'post', {id_post, id_user}, { headers: authHeader()});
  }

  removePostLike(id_post, id_user) {
    return axios.delete(API_URL + 'post', { data: {id_post, id_user}, headers: authHeader() });
  }
}
  
export default new LikeService();