module.exports = (sequelize, Sequelize) => {
    const Post = sequelize.define("post", {
        text: {
            type: Sequelize.STRING
        },
        image: {
            type: Sequelize.STRING
        }
    });
    return Post;
};