import store from '../store'

export default function authHeader() {
  let user = store.state.auth.user;

  if (user && user.accessToken) {
    return {
      Authorization: 'Bearer ' + user.accessToken
    };
  } else {
    return {};
  }
}