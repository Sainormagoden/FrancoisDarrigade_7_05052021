require('dotenv').config();
const express = require('express');
const postRoutes = require('./routes/post');
const userRoutes = require('./routes/user');
const commentRoutes = require('./routes/comment');
const forumRoutes = require('./routes/forum');
const likeRoutes = require('./routes/like');
const path = require('path');
const helmet = require('helmet');
const session = require('express-session');
const config = require('./config.js');
const app = express();
const utils = require('./utils');
const bcrypt = require('bcrypt');

// Add datas in header at responses for CORS.
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*'); 
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
app.use(helmet());

// Cookie configuration.
app.set('trust proxy', 1) // trust first proxy
app.use( session({
    secret : config.SESSION_SECRET,
    name : config.SESSION_NAME,
    resave: true,
    saveUninitialized: true,
    httpOnly: true
    })
);

// For parsing application/xwww-.
app.use(express.urlencoded({ extended: false }));

// For parsing application/json.
app.use(express.json());

// for parsing multipart/form-data
app.use('/images', express.static(path.join(__dirname, 'images')));

//////////// ALL PATH REQUEST./////////////

app.use('/api/user', userRoutes);
app.use('/api/post', postRoutes);
app.use('/api/comment', commentRoutes);
app.use('/api/forum', forumRoutes);
app.use('/api/like', likeRoutes);

//////////// ALL PATH REQUEST END./////////////

// Add Configuration at Sequelize.
const db = require("./models");
if(config.DB_INIT == 'true') {
    db.sequelize.sync({force: true}).then(async () => {
        db.roles.create({ 'id' : '1', 'role_name' : 'User' });
        db.roles.create({ 'id' : '2', 'role_name' : 'Admin' });
        
        db.forums.create({ 'id' : '1', 'forum_name' : 'Text-Only' });
        db.forums.create({ 'id' : '2', 'forum_name' : 'Image-Only' });
        db.forums.create({ 'id' : '3', 'forum_name' : 'Mix' });

        db.users.create({ 'id' : 1, 'firstname' : 'Admin', 'lastname' : 'User', 'email' : utils.encrypt('admin@groupomania.com'), 'email_mask' : utils.maskEmail('admin@groupomania.com'), 'password' :  await bcrypt.hash('Test1234', 10), 'id_role' : 2, 'user_image' : config.DEFAULT_USER_IMAGE });
        db.users.create({ 'id' : 2, 'firstname' : 'Simple', 'lastname' : 'User', 'email' : utils.encrypt('user@groupomania.com'), 'email_mask' : utils.maskEmail('user@groupomania.com'), 'password' :  await bcrypt.hash('Test1234', 10), 'user_image' : config.DEFAULT_USER_IMAGE }); 

        db.posts.create({ 
            'text' : 'Nullam scelerisque, enim id semper consectetur, massa metus aliquam tellus, at volutpat turpis enim eu libero. In suscipit consequat eros, sed pharetra mi volutpat in. Vestibulum ante dolor, viverra vel porttitor ut, iaculis ac mi. Nam id pretium erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut turpis lorem. Fusce sed porttitor dolor, eu faucibus urna.',
            'id_forum' : '1', 'id_user' : '1'
        });
        db.posts.create({ 
            'text' : 'Nullam scelerisque, enim id semper consectetur, massa metus aliquam tellus, at volutpat turpis enim eu libero. In suscipit consequat eros, sed pharetra mi volutpat in. Vestibulum ante dolor, viverra vel porttitor ut, iaculis ac mi. Nam id pretium erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut turpis lorem. Fusce sed porttitor dolor, eu faucibus urna.',
            'id_forum' : '1', 'id_user' : '2'
        });
        db.posts.create({ 
            'image' : 'https://picsum.photos/1920/1080',
            'id_forum' : '2', 'id_user' : '1'
        });
        db.posts.create({ 
            'text' : 'Nullam scelerisque, enim id semper consectetur, massa metus aliquam tellus, at volutpat turpis enim eu libero. In suscipit consequat eros, sed pharetra mi volutpat in. Vestibulum ante dolor, viverra vel porttitor ut, iaculis ac mi. Nam id pretium erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut turpis lorem. Fusce sed porttitor dolor, eu faucibus urna.',
            'image' : 'https://picsum.photos/900',
            'id_forum' : '3', 'id_user' : '1'
        });
    });
} else {
    db.sequelize.sync().then(() => {
        console.log('database sync');
    })
}

module.exports = app;