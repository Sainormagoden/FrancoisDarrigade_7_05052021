const express = require('express');
const router = express.Router();
const likeCtrl = require('../controllers/like');
const auth = require('../middleware/auth');

// Add like on post.
router.post('/post', auth, likeCtrl.addPostLike);

// Remove like on post.
router.delete('/post', auth, likeCtrl.removePostLike);

module.exports = router;