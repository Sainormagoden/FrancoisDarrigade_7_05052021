const db = require("../models");
const Forum = db.forums;
const Post = db.posts;
const Sequelize = db.Sequelize;

/**
 * Get all forum.
 * @returns {Object || String} Return forum's data if success or a error.
 */
exports.findAll = () => {  
      return Forum.findAll({ subQuery: false, attributes:{ 
                  include: [[Sequelize.literal('(SELECT COUNT(*) FROM posts Where posts.id_forum = forum.id)'), "countPosts"]]
            },
            include: [{
                  model: Post,
                  attributes: []
            }]
      })
            .then(data => {
                  return data ;
            })
            .catch(err => {
                  throw err.message || "Some error occurred while retrieving forums.";
            });
};