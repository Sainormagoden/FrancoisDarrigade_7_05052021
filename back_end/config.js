module.exports = {
    DB_HOST : process.env.DB_HOST,
    DB_USER : process.env.DB_USER,
    DB_PASS : process.env.DB_PASS,
    DB_NAME : process.env.DB_NAME,
    DB_INIT : process.env.DB_INIT,

    JWT_TOKEN : process.env.JWT_TOKEN,
    SESSION_SECRET : process.env.SESSION_SECRET,
    SESSION_NAME : process.env.SESSION_NAME,
    CRYPTO_SECRET : process.env.CRYPTO_SECRET,
    CRYPTO_IV : process.env.CRYPTO_IV,

    AWS_ACCESS_KEY_ID : process.env.AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY : process.env.AWS_SECRET_ACCESS_KEY,
    AWS_BUCKET_NAME : process.env.AWS_BUCKET_NAME,

    DEFAULT_USER_IMAGE : process.env.DEFAULT_USER_IMAGE
}