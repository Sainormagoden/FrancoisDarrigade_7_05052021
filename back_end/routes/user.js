const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user');
const auth = require('../middleware/auth');
const verifData = require('../middleware/verifUserData');
const verifRightUser = require('../middleware/verifRightUser');

// Sign a new user.
router.post('/signup', verifData, userCtrl.create);

// Log user.
router.post('/login', userCtrl.logUser);

// Get one user.
router.get('/:id', auth, userCtrl.findOne);

// Modify a user.
router.put('/:id', auth, verifRightUser.check('User'), verifData, userCtrl.update);

// Delete a user.
router.delete('/:id', auth, verifRightUser.check('User'), userCtrl.delete);

module.exports = router;