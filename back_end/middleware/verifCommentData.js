const constant = require('../constant.js');

/**
 * Check if post data is correct
 */
module.exports = (req, res, next) => {
    try {
        
        // Validate request.
        if (!req.body.text && !req.body.image) {
            return res.status(400).json({succes: false, message: "Invalid parameters provided !"});
        }
        next();
    }
    catch {
        res.status(403).json({ error: 'Invalid request!' });
    }
};