module.exports = (sequelize, Sequelize) => {
    const Forum = sequelize.define("forum", {
        forum_name: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        }
    });
    return Forum;
};