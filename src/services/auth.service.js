import axios from 'axios';
import config from '../config/config';

const API_URL = config.API_URL+'user/';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'login', {
        email: user.email,
        password: user.password
      })
      .then(response => {
        if (response.data.message.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data.message));
        }

        return response.data.message;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      password: user.password
    });
  }
}

export default new AuthService();