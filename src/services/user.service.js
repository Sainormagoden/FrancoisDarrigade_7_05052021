import axios from 'axios';
import authHeader from './auth-header';
import config from '../config/config';

const API_URL = config.API_URL+'user/';

class UserService {
  getOne(id) {
    return axios.get(API_URL + id, { headers: authHeader() });
  }

  update(id, user, image = null) {
    if (image != null) {
      return new Promise ((resolve, reject) => {
        axios.post(config.API_URL+'post/' + 'getSignedUrl', {'image' : Date.now() + image.name}, { headers: authHeader()})
          .then((url) => {
            axios.put(url.data.message.putUrl, image)
              .then(() => {
                user.image = url.data.message.getUrl;
                return resolve(axios.put(API_URL + id, user, { headers: authHeader()}));
              })
              .catch((err) => {
                reject(err);
              })
          })
          .catch((err) => {
            reject(err);
          })
      })
    } else {
      return axios.put(API_URL + id, user, { headers: authHeader()});
    }
  }

  delete(id) {
    return axios.delete(API_URL + id, { headers: authHeader() });
  }
}  
export default new UserService();