const db = require("../models");
const Post = db.posts;
const User = db.users;
const Comment = db.comments;
const utils = require('../utils');
const Sequelize = db.Sequelize;

/**
 * Get signed url for JPG, PNG or GIF.
 * @param {String} image_name Name of image. 
 * @returns {Object || String} Return a object with two url, for get and put image, if success or a error message.
 */
exports.getSignedUrl = (image_name) => {
    return utils.getSignedUrl(image_name.replace(/[^A-Z0-9.]+/ig, "_"))
        .then((url) => {
            return {
                putUrl : url,
                getUrl : url.split('?')[0]
            };
        })
        .catch((err) => {
            throw err.message || "Some error occurred while getting url";
        })
}

/**
 * Create and Save a new Post.
 * @param {Object} post Object content all data for a post.
 * @param {String} post.text Text of post.
 * @param {String} post.image Link for image.
 * @param {Number} post.id_forum ID of forum for post.
 * @param {Number} post.id_user ID of user who create a post.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.create = (post) => {
    return Post.create(post)
        .then(() => {
            return 'Save post';
        })
        .catch(err => {
            throw err.message || "Some error occurred while creating the post.";
        });
};

/**
 * Retrieve all Posts from the database.
 * @param {String} condition Add condition.
 * @returns {Object || String} Return all post in a object if succes or a error.
 */
exports.findAll = (condition) => {  
    return Post.findAll({ where: condition, order: [['createdAt', 'DESC'], ['id', 'DESC']], subQuery: false, attributes:{ 
            include: [
                [Sequelize.literal('(SELECT COUNT(*) FROM comments WHERE post.id = comments.id_post)'), "countComments"], 
                [Sequelize.literal('(SELECT COUNT(*) FROM post_likes WHERE post.id = post_likes.id_post)'), "countLiked"]
            ]
        },
        include: [{
            model: User,
            as: 'user',
            attributes: ['firstname', 'lastname', 'id', 'user_image']
        }, {
            model: User,
            as: 'users_liked',
            attributes: ['id']
        }, { 
            model: Comment, 
            limit: 2, 
            include: [{
                model: User,
                as: 'user',
                attributes: ['firstname', 'lastname', 'id', 'user_image']
            }]
        }]
    })
        .then(data => {
            return data;
        })
        .catch(err => {
            throw err.message || "Some error occurred while retrieving posts.";
        });
};

/**
 * Retrieve a limited number at Posts from the database.
 * @param {Number} limit Add limit.
 * @returns {Object || String} Return posts in a object if succes or a error.
 */
exports.findLimit = (limit) => {  
    return Post.findAll({ limit, order: [['createdAt', 'DESC'], ['id', 'DESC']],
        include: [{
            model: User,
            as: 'user',
            attributes: ['firstname', 'lastname', 'id', 'user_image']
        }]})
        .then(data => {
            return data;
        })
        .catch(err => {
            throw err.message || "Some error occurred while retrieving posts.";
        });
};

/**
 * Find a single Post with an id.
 * @param {Number} id ID of post.
 * @returns {Object || String} Return post in a object if succes or a error.
 */
exports.findOne = (id) => {
    return Post.findByPk(id, { subQuery: false, attributes:{ 
            include: [
                [Sequelize.literal('(SELECT COUNT(*) FROM comments WHERE post.id = comments.id_post)'), "countComments"], 
                [Sequelize.literal('(SELECT COUNT(*) FROM post_likes WHERE post.id = post_likes.id_post)'), "countLiked"]
            ]
        },
        include: [{ 
            model: User,
            as: 'user',
            attributes: ['firstname', 'lastname', 'id', 'user_image'] 
        }, {
            model: User,
            as: 'users_liked',
            attributes: ['id']
        }, {
            model: Comment, 
            include: [{
                model: User,
                as: 'user',
                attributes: ['firstname', 'lastname', 'id', 'user_image']
            }]
        }]
    })
        .then(data => {
            return data;
        })
        .catch(err => {
            throw err.message || "Error retrieving Post with id=" + id;
        });      
};

/**
 * Update a Post by the id in the request.
 * @param {Number} id ID of post.
 * @param {Object} post Object content all data for a post.
 * @param {String} post.text Text of post.
 * @param {String} post.image Link for image.
 * @param {Number} post.id_forum ID of forum for post.
 * @param {Number} post.id_user ID of user who create a post.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.update = (id, post) => {    
    return Post.findByPk(id)
        .then(data => {
            return Post.update(post, {where: { id: id }})
                .then(num => {
                    if (num == 1) {                                                                   
                        if (data.image && post.image != data.image && data.image.includes("amazonaws.com/")) {
                            utils.deleteObject(data.image.split("amazonaws.com/")[1]);
                        }
                        return "Post was updated successfully.";
                    } else {
                        throw `Cannot update Post with id=${id}. Maybe Post was not found or req.body is empty!`;
                    }
                })
                .catch(err => {
                    throw err.message || "Error updating Post with id=" + id
                });
        })
        .catch(err => {
            throw err.message || "Error retrieving Post with id=" + id
        });      
};

/**
 * Delete a Post with the specified id in the request.
 * @param {Number} id ID of post.
 * @param {String} post_image Link of image to delete.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.delete = async (id, post_image) => {
    
    
    // Find user's comments for delete image.
    await Comment.findAll({where: { id_post: id }})
        .then((comments) => {
            comments.forEach(comment => {
                if (comment.image && comment.image.includes("amazonaws.com/")) {
                    utils.deleteObject(comment.image.split("amazonaws.com/")[1]);
                } 
            });
        })

    // Find user and delete it and her image.
    return Post.destroy({where: { id: id }})
        .then(num => {
            if (num == 1) {
                if (post_image && post_image.includes("amazonaws.com/")) {
                    utils.deleteObject(post_image.split("amazonaws.com/")[1]);
                }
                return "Post was deleted successfully!";
            } else {
                throw `Cannot delete Post with id=${id}. Maybe Post was not found!`
            }
        })
        .catch(err => {
            throw err.message || "Could not delete Post with id=" + id;
        });
};