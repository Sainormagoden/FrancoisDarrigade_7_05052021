import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
const axios = require('axios')

createApp(App).use(store).use(router).mount('#app')

axios.interceptors.response.use(
    function (response) {
        return response
    }, function (error) {
        console.log(error.response.data)
        if (error.response.status === 401) {
            store.dispatch('auth/logout')
            router.push('/login')
            router.go(0)
        }
        return Promise.reject(error)
    }
)