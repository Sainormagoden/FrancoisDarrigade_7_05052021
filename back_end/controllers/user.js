const constant = require('../constant.js');
const userService = require('../services/user');

// Create and Save a new Users.
exports.create = async (req, res) => {

    // Validate request.
    if (!req.body.email || !req.body.password) {
        return res.status(400).json({succes: false, message: "Invalid parameters provided !"});
    }
  
    try {   
        const user = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email:req.body.email,
            password: req.body.password
        }         
        const response = await userService.create(user);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    }
};

// Find a single Users with an id
exports.findOne = async (req, res) => {
    try {            
        const response = await userService.findOne(req.params.id);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(404).json({
            succes: false, message: err
        });
    }
};

// Update a Users by the id in the request
exports.update = async (req, res) => {

    // Validate request.
    if (!req.body.email) {
        return res.status(400).json({succes: false, message: "Invalid parameters provided !"});
    }

    try {            
        const response = await userService.update(req.params.id, req.body);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(404).json({
            succes: false, message: err
        });
    }
};

// Delete a Users with the specified id in the request
exports.delete = async (req, res) => {
    try {            
        const response = await userService.delete(req.params.id);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    }
};

/**
 * Check the information identification of the user.
 * Send user ID and Token
 */ 
exports.logUser = async (req, res) => {
    try {            
        const response = await userService.logUser(req.body.email, req.body.password);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    }
};