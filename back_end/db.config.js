const config = require('./config.js');

module.exports = {
    HOST: config.DB_HOST, 
    PORT: 3306,
    USER: config.DB_USER,
    PASSWORD: config.DB_PASS,
    DB: config.DB_NAME,
    dialect: "mysql",
    dialectOptions: {
        ssl:'Amazon RDS'
    }, 
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    language: 'fr'
};