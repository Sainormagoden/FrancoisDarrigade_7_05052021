exports.dateFormat = (oldDate, isFull = true) => {
    const date = new Date(oldDate);
    const year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();
    let hh = date.getHours();
    let mm = date.getMinutes();

    
    if (month < 10) {
        month = '0' + month;
    }
    if (dt < 10) {
        dt = '0' + dt;
    }
    if (hh < 10) {
        hh = '0' + hh;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    const dateString = (isFull === true) ? dt + '/' + month + '/'+ year + ' ' + hh + ':' + mm : dt + '/' + month + '/'+ year;
    return dateString;
}