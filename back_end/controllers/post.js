const postService = require('../services/post');

// Get signed url.
exports.getSignedUrl = async (req, res) => {
      try {            
            const response = await postService.getSignedUrl(req.body.image);
            res.status(201).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      }
}

// Create and Save a new Post
exports.create = async (req, res) => {
      try {
            const post = {
                  text: req.body.text,
                  image: req.body.image,
                  id_forum: req.body.id_forum,
                  id_user: req.body.id_user
            };
            const response = await postService.create(post);
            res.status(201).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      };
};

// Retrieve all Posts from the database.
exports.findAll = async (req, res) => {
      try { 
            const id_forum = req.params.id_forum;
            const condition = id_forum ? { id_forum } : null;           
            const response = await postService.findAll(condition);
            res.status(200).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      }
};

// Retrieve a limited number at Posts from the database.
exports.findLimit = async (req, res) => {
      try {            
            const limit = parseInt(req.params.limit);
            const condition = limit ? limit : 5;
            const response = await postService.findLimit(condition);
            res.status(200).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      }
};

// Find a single Post with an id
exports.findOne = async (req, res) => {
      try {
            const response = await postService.findOne(req.params.id);
            res.status(200).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      }
};

// Update a Post by the id in the request
exports.update = async (req, res) => {
      try {
            const response = await postService.update(req.params.id, req.body);
            res.status(200).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      }
};

// Delete a Post with the specified id in the request
exports.delete =  async (req, res) => {
      try {
            const response = await postService.delete(req.params.id, req.body.image);
            res.status(200).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      }
};