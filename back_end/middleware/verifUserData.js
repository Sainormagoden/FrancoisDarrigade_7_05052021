const constant = require('../constant.js');

/**
 * Check if post data is correct
 */
module.exports = (req, res, next) => {
    try {
        
        // Test if good password and mail.
        if (req.body.password && !constant.regexPassword.test(req.body.password)) {
            return res.status(403).json({ succes: false, message: 'Password must contain at least 8 characters, including 1 uppercase, 1 lowercase and 1 number.' });
        }
        if (req.body.email && !constant.regexMail.test(req.body.email)) {
            return res.status(403).json({ succes: false, message: 'Incorrect email !' });
        }
        next();
    }
    catch {
        res.status(403).json({ error: 'Invalid request!' });
    }
};