const likeService = require('../services/like');

// Add like on post.
exports.addPostLike = async (req, res) => {
    try {
        const response = await likeService.addPostLike(req.body.id_post, req.body.id_user);
        res.status(201).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    };
};

// Remove like on post.
exports.removePostLike = async (req, res) => {
    try {            
        const response = await likeService.removePostLike(req.body.id_post, req.body.id_user);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    };
};