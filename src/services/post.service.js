import axios from 'axios';
import authHeader from './auth-header';
import config from '../config/config';

const API_URL = config.API_URL+'post/';

class PostService {
  getAll(forumId = '') {
    return axios.get(API_URL+forumId, { headers: authHeader() });
  }

  getRecent(limit = 5) {
    return axios.get(API_URL + 'recentPost/' + limit, { headers: authHeader() });
  }

  getOne(postId) {
    return axios.get(API_URL + 'post/' + postId, { headers: authHeader() });
  }

  delete(id, path = null) {
    return axios.delete(API_URL + id, { data: {'image' : path}, headers: authHeader() });
  }

  create(post, image = null) {
    if (image != null) {
      return new Promise ((resolve, reject) => {
        axios.post(API_URL + 'getSignedUrl', {'image' : Date.now() + image.name}, { headers: authHeader()})
          .then((url) => {
            axios.put(url.data.message.putUrl, image)
              .then(() => {
                post.image = url.data.message.getUrl;
                return resolve(axios.post(API_URL, post, { headers: authHeader()}));
              })
              .catch((err) => {
                reject(err);
              })
          })
          .catch((err) => {
            reject(err);
          })
      })
    } else {
      return axios.post(API_URL, post, { headers: authHeader()});
    }
  }

  update(id, post, image = null) {
    if (image != null) {
      return new Promise ((resolve, reject) => {
        axios.post(API_URL + 'getSignedUrl', {'image' : Date.now() + image.name}, { headers: authHeader()})
          .then((url) => {
            axios.put(url.data.message.putUrl, image)
              .then(() => {
                post.image = url.data.message.getUrl;
                return resolve(axios.put(API_URL + id, post, { headers: authHeader()}));
              })
              .catch((err) => {
                reject(err);
              })
          })
          .catch((err) => {
            reject(err);
          })
      })
    } else {
      return axios.put(API_URL + id, post, { headers: authHeader()});
    }
  }
}
  
export default new PostService();