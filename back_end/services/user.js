const db = require("../models");
const User = db.users;
const Role = db.roles;
const Post = db.posts;
const Comment = db.comments;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config.js');
const constant = require('../constant.js');
const utils = require('../utils');

/**
 * Create and Save a new Users.
 * @param {Object} dataUser All basics data for user.
 * @param {String} dataUser.firstname Firstname of user.
 * @param {String} dataUser.lastname Lastname of user.
 * @param {String} dataUser.password Password of user.
 * @param {String} dataUser.email Email of user.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.create = (dataUser) => {

    // Crypt password.
    return bcrypt.hash(dataUser.password, 10)
        .then(hash => {
            const user = {
                firstname: dataUser.firstname,
                lastname: dataUser.lastname,
                email: utils.encrypt(dataUser.email),
                email_mask: utils.maskEmail(dataUser.email),
                password: hash,
                user_image: config.DEFAULT_USER_IMAGE,
            };

            // Save User in the database
            return User.create(user)
                .then(() => {
                    return 'Save user';
                })
                .catch(err => {
                    throw err.message || "Some error occurred while creating the User.";
                });
        })
        .catch(error => { throw error.message });
};

/**
 * Find a single Users with an id.
 * @param {Number} id ID of user.
 * @returns {Object || String} Return user in a object if succes or a error.
 */
exports.findOne = (id) => {
    return User.findByPk(id)
        .then(data => {
            return {
                id: data.id,
                firstname: data.firstname,
                lastname: data.lastname,
                email: utils.decrypt(data.email),
                user_image: data.user_image,
                birth: data.birth,
                job: data.job
            };
        })
        .catch(err => {
            throw err.message || "Error retrieving User with id=" + id;
        });
};

/**
 * Update a Users by the id in the request.
 * @param {Number} id ID of user.
 * @param {Object} dataUser All basics data for user.
 * @param {String} dataUser.firstname Firstname of user.
 * @param {String} dataUser.lastname Lastname of user.
 * @param {String} dataUser.password Password of user.
 * @param {String} dataUser.email Email of user.
 * @param {Date} dataUser.birth Birth of user.
 * @param {String} dataUser.job Job of user.
 * @param {String} dataUser.image link at image of user.
 * @returns {Object || String} Return user in a object if succes or a error.
 */
exports.update = async (id, dataUser) => {
    let user = {
        firstname: dataUser.firstname,
        lastname: dataUser.lastname,
        email: utils.encrypt(dataUser.email),
        email_mask: utils.maskEmail(dataUser.email),
        birth: dataUser.birth,
        job: dataUser.job,
        user_image: (dataUser.image) ? dataUser.image : config.DEFAULT_USER_IMAGE
    };

    // Test password.
    if (dataUser.password) {
        if (dataUser.password_repeat && dataUser.password_repeat === dataUser.password) {       
            if (constant.regexPassword.test(dataUser.password)) {    
                user.password = await bcrypt.hash(dataUser.password, 10);
            } else {throw 'Password must contain at least 8 characters, including 1 uppercase, 1 lowercase and 1 number.'}
        } else {throw 'Passwords must match'} 
    }
    
    return User.findByPk(id)
        .then(data => {
            return User.update(user, {where: { id: id }})
                .then(num => {
                    if (num == 1) {                               
                        if (data.user_image != config.DEFAULT_USER_IMAGE && user.user_image != data.user_image && data.user_image.includes("amazonaws.com/")) {
                            utils.deleteObject(data.user_image.split("amazonaws.com/")[1]);
                        }
                        return user;
                    } else {
                        throw `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`;
                    }
                })
                .catch(err => {
                    throw err.message || "Error updating User with id=" + id;
                });
        })
        .catch(err => {
            throw err.message || "Error retrieving User with id=" + id;
        });     
};

/**
 * Delete a Users with the specified id in the request.
 * @param {Number} id ID of user.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.delete = async (id) => {

    // Find user's comments for delete image.
    await Comment.findAll({where: { id_user: id }})
        .then((comments) => {
            comments.forEach(comment => {
                if (comment.image && comment.image.includes("amazonaws.com/")) {
                    utils.deleteObject(comment.image.split("amazonaws.com/")[1]);
                } 
            });
        })

    // Find user's posts for delete image.
    await Post.findAll({where: { id_user: id }})
        .then((posts) => {
            posts.forEach(post => {
                if (post.image && post.image.includes("amazonaws.com/")) {
                    utils.deleteObject(post.image.split("amazonaws.com/")[1]);
                } 
            });
        })

    // Find user and delete it and her image.
    return User.findByPk(id)
        .then(data => {
            return User.destroy({where: { id: id }})
                .then(num => {
                    if (num == 1) {
                        if (data.user_image != config.DEFAULT_USER_IMAGE && data.user_image.includes("amazonaws.com/")) {
                            utils.deleteObject(data.user_image.split("amazonaws.com/")[1]);
                        }
                        return "User was deleted successfully!";
                    } else {
                    throw `Cannot delete User with id=${id}. Maybe User was not found!`;
                    }
                })
                .catch(err => {
                    throw err.message || "Could not delete User with id=" + id;
                });
        })
        .catch(err => {
            throw err.message || "Error retrieving User with id=" + id;
        });
};

/**
 * Check the information identification of the user.
 * Send user ID and Token.
 * @param {*} email Email of user.
 * @param {*} password Password of user.
 * @returns {Object || String} Return user in a object if succes or a error.
 */
 exports.logUser = (email, password) => {
    return User.findOne({where: {email: utils.encrypt(email)}, include: [Role]})
        .then(user => {
            if (!user) {
                throw 'User not found !';
            }
            return bcrypt.compare(password, user.password)
                .then(valid => {
                    if (!valid) {
                        throw 'Incorrect password !';
                    }
                    const accessToken = jwt.sign({ userId: user.id, userRole: user.role.role_name }, config.JWT_TOKEN);
                    return {
                        userId: user.id,
                        accessToken,
                        firstname: user.firstname,
                        lastname: user.lastname,
                        role: user.role.role_name,
                        user_image: user.user_image
                    };
                })
                .catch(error => {throw error.message });
        })
        .catch(err => {
            throw err.message || 'User not found !';
        });
};