const express = require('express');
const router = express.Router();
const forumCtrl = require('../controllers/forum');
const auth = require('../middleware/auth');

// Get all forums.
router.get('/', auth, forumCtrl.findAll);

module.exports = router;