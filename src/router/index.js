import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Profile from '../views/Profile.vue'
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import PathNotFound from '../components/PathNotFound.vue'
import store from '../store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/forum/:id',
    name: 'Forum',
    component: Home
  },
  {
    path: '/post/:id_post',
    name: 'post',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile
  },
  {
    path: '/profile/:setting',
    name: 'profileSetting',
    component: Profile
  },
  { 
    path: '/:pathMatch(.*)*',
    component: PathNotFound 
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = store.state.auth.user;

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && loggedIn == null) {
    next('/login');
  } else {
    next();
  }
});

export default router
