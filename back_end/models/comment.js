module.exports = (sequelize, Sequelize) => {
    const Comment = sequelize.define("comment", {
        text: {
            type: Sequelize.STRING
        },
        image: {
            type: Sequelize.STRING
        }
    });
    return Comment;
};