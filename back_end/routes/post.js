const express = require('express');
const router = express.Router();
const postCtrl = require('../controllers/post');
const auth = require('../middleware/auth');
const verifData = require('../middleware/verifPostData');
const verifRightUser = require('../middleware/verifRightUser');

// Get all posts.
router.get('/', auth, postCtrl.findAll);
router.get('/:id_forum', auth, postCtrl.findAll);

// Get one post.
router.get('/post/:id', auth, postCtrl.findOne);

// Get one post with limit.
router.get('/recentPost/:limit', auth, postCtrl.findLimit);

// Get url for image.
router.post('/getSignedUrl', auth, postCtrl.getSignedUrl);

// Create a post.
router.post('/', auth, verifData, postCtrl.create);

// Modify a post.
router.put('/:id', auth, verifRightUser.check('Post'), verifData, postCtrl.update);

// Delete a post.
router.delete('/:id', auth, verifRightUser.check('Post'), postCtrl.delete);

module.exports = router;