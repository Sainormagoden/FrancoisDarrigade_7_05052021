const db = require("../models");
const Comment = db.comments;
const utils = require('../utils');

/**
 * Create and Save a new Comment.
 * @param {Object} comment Object content all data for a comment.
 * @param {String} comment.text Text of comment.
 * @param {String} comment.image Link for image.
 * @param {Number} comment.id_post ID of post at comment.
 * @param {Number} comment.id_user ID of user who post a comment.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.create = (comment) => {
    return Comment.create(comment)
        .then(() => {
            return 'Save comment';
        })
        .catch(err => {
            throw err.message || "Some error occurred while creating the comment.";
        });
};

/**
 * Update a Comment.
 * @param {Number} id ID of comment.
 * @param {Object} comment Object content all data for a comment.
 * @param {String} comment.text Text of comment.
 * @param {String} comment.image Link for image.
 * @param {Number} comment.id_post ID of post at comment.
 * @param {Number} comment.id_user ID of user who post a comment.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.update = (id, comment) => {
    return Comment.findByPk(id)
        .then(data => {
            return Comment.update(comment, {where: { id: id }})
                .then(num => {
                    if (num == 1) {                                                                   
                        if (data.image && comment.image != data.image && data.image.includes("amazonaws.com/")) {
                            utils.deleteObject(data.image.split("amazonaws.com/")[1]);
                        }
                        return "comment was updated successfully.";
                    } else {
                        throw `Cannot update User with id=${id}. Maybe comment was not found or req.body is empty!`;
                    }
                })
                .catch(err => {
                    throw err.message || "Error updating comment with id=" + id;
                });
        })
        .catch(err => {
            throw err.message || "Error retrieving comment with id=" + id;
        });     
};

/**
 * Delete a Comment.
 * @param {Number} id ID of comment.
 * @param {String} comment_image Link for image.
 * @returns {String} Return a simple text if succes or a error.
 */
exports.delete = (id, comment_image) => {
    return Comment.destroy({where: { id: id }})
        .then(num => {
            if (num == 1) {
                if (comment_image && comment_image.includes("amazonaws.com/")) {
                    utils.deleteObject(comment_image.split("amazonaws.com/")[1]);
                }
                return "comment was deleted successfully!";
            } else {
                throw `Cannot delete comment with id=${id}. Maybe User was not found!`
            }
        })
        .catch(err => {
            return err.message || "Could not delete comment with id=" + id;
        });
};