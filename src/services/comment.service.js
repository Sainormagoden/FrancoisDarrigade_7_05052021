import axios from 'axios';
import authHeader from './auth-header';
import config from '../config/config';

const API_URL = config.API_URL+'comment/';

class CommentService {

  delete(id, path = null) {
    return axios.delete(API_URL + id, { data: {'image' : path}, headers: authHeader() });
  }

  create(id_post, comment, image = null) {
    if (image != null) {
      return new Promise ((resolve, reject) => {
        axios.post(config.API_URL+'post/' + 'getSignedUrl', {'image' : Date.now() + image.name}, { headers: authHeader()})
          .then((url) => {
            axios.put(url.data.message.putUrl, image)
              .then(() => {
                comment.image = url.data.message.getUrl;
                return resolve(axios.post(API_URL + id_post, comment, { headers: authHeader()}));
              })
              .catch((err) => {
                reject(err);
              })
          })
          .catch((err) => {
            reject(err);
          })
      })
    } else {
      return axios.post(API_URL + id_post, comment, { headers: authHeader()});
    }
  }

  update(id, comment, image = null) {
    if (image != null) {
      return new Promise ((resolve, reject) => {
        axios.post(config.API_URL+'post/' + 'getSignedUrl', {'image' : Date.now() + image.name}, { headers: authHeader()})
          .then((url) => {
            axios.put(url.data.message.putUrl, image)
              .then(() => {
                comment.image = url.data.message.getUrl;
                return resolve(axios.put(API_URL + id, comment, { headers: authHeader()}));
              })
              .catch((err) => {
                reject(err);
              })
          })
          .catch((err) => {
            reject(err);
          })
      })
    } else {
      return axios.put(API_URL + id, comment, { headers: authHeader()});
    }
  }
}
  
export default new CommentService();