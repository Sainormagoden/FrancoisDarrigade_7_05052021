const dbConfig = require("../db.config.js");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});
 
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.comments = require("./comment.js")(sequelize, Sequelize);
db.forums = require("./forum.js")(sequelize, Sequelize);
db.posts = require("./post.js")(sequelize, Sequelize);
db.roles = require("./role.js")(sequelize, Sequelize);
db.users = require("./user.js")(sequelize, Sequelize);

//////////// ONE TO MANY./////////////

// Users and roles.
db.roles.hasMany(db.users, {
  foreignKey: {
    name: 'id_role',
    allowNull: false,
    defaultValue: 1,
    onDelete: 'RESTRICT',
    onUpdate: 'SET NULL'
  }
});
db.users.belongsTo(db.roles, {
  foreignKey: {
    name: 'id_role',
    allowNull: false,
    defaultValue: 1,
    onDelete: 'RESTRICT',
    onUpdate: 'SET NULL'
  }
});

// Forums and posts.
db.forums.hasMany(db.posts, {
  foreignKey: {
    name: 'id_forum',
    allowNull: false,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});
db.posts.belongsTo(db.forums, {
  foreignKey: {
    name: 'id_forum',
    allowNull: false,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});

// Users and posts.
db.users.hasMany(db.posts, {
  foreignKey: {
    name: 'id_user',
    allowNull: false,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});
db.posts.belongsTo(db.users, {
  foreignKey: {
    name: 'id_user',
    allowNull: false,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});

// Users and comments.
db.users.hasMany(db.comments, {
  foreignKey: {
    name: 'id_user',
    allowNull: false,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});
db.comments.belongsTo(db.users, {
  foreignKey: {
    name: 'id_user',
    allowNull: false,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});

// Posts and comments.
db.posts.hasMany(db.comments, {
  foreignKey: {
    name: 'id_post',
    allowNull: true,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});
db.comments.belongsTo(db.posts, {
  foreignKey: {
    name: 'id_post',
    allowNull: true,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});

//////////// ONE TO MANY END./////////////

//////////// MANY TO MANY./////////////

// Posts and users for likes.
db.users.belongsToMany(db.posts, {
  through: "post_likes",
  as: 'posts_liked',
  foreignKey: {
    name: 'id_user',
    allowNull: true,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});
db.posts.belongsToMany(db.users, {
  through: "post_likes",
  as: 'users_liked',
  foreignKey: {
    name: 'id_post',
    allowNull: true,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});

db.users.belongsToMany(db.posts, {
  through: "post_likes",
  foreignKey: {
    name: 'id_user',
    allowNull: true,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});
db.posts.belongsToMany(db.users, {
  through: "post_likes",
  foreignKey: {
    name: 'id_post',
    allowNull: true,
    onDelete: 'CASCADE',
    onUpdate: 'SET NULL'
  }
});
//////////// MANY TO MANY END./////////////

module.exports = db;