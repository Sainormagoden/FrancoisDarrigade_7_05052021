const commentService = require('../services/comment');

// Create and Save a new Comment
exports.create = async (req, res) => {
    try {            
        const id_post = req.params.id_post;
        const comment = {
            text: req.body.text,
            image: req.body.image,
            id_post,
            id_user: req.body.id_user
        };
        const response = await commentService.create(comment);
        res.status(201).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    };
};

// Update a Comment by the id in the request
exports.update = async (req, res) => {
    try {
        const id = req.params.id;
        const comment = req.body;
        const response = await commentService.update(id, comment);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    };
};

// Delete a Comment with the specified id in the request
exports.delete = async (req, res) => {
    try {            
        const id = req.params.id;
        const comment_image = req.body.image;
        const response = await commentService.delete(id, comment_image);
        res.status(200).json({ succes: true, message: response });
    } catch (err) {
        res.status(500).json({
            succes: false, message: err
        });
    };
};