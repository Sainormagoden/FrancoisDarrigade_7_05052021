const forumService = require('../services/forum');

// Retrieve all Posts from the database.
exports.findAll = async (req, res) => {
      try {            
            const response = await forumService.findAll();
            res.status(200).json({ succes: true, message: response });
      } catch (err) {
            res.status(500).json({
                  succes: false, message: err
            });
      }
};