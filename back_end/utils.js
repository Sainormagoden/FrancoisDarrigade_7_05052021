const fs = require('fs');
const config = require('./config.js');
const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const secretKey = crypto.createHash('sha256').update(String(config.CRYPTO_SECRET)).digest('base64').substr(0, 32);
const iv = crypto.createHash('sha256').update(String(config.CRYPTO_IV)).digest('base64').substr(0, 16);
const MaskData = require('maskdata');

const db = require("./models");
const User = db.users;
const Role = db.roles;

const AWS = require('aws-sdk');
const bucket_name = config.AWS_BUCKET_NAME;
const s3bucket = new AWS.S3({
    accessKeyId: config.AWS_ACCESS_KEY_ID,
    secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
    region : "eu-west-3",
});

/**
 * Create a bucket S3.
 * @returns {void} Return a console.log message.
 */
exports.createS3 = () => {
    const params = {
        Bucket: bucket_name,
        CreateBucketConfiguration: {
            LocationConstraint: "eu-west-3"
        }
    };
    s3bucket.createBucket(params, function (err, data) {
        if (err) console.log(err, err.stack);
        else console.log('Bucket Created Successfully', data.Location);
    });
}

/**
 * Create a bucket S3.
 * @param {string} filename file to upload.
 * @returns {string || object} Return the location at file or a error object.
 */
exports.getSignedUrl = (filename) => {
    const params = {
        Bucket: bucket_name,
        Key: filename, 
        Expires: 2*60,
        ACL: 'public-read'
    };
 
    // Uploading files to the bucket
    return new Promise ((resolve, reject) => {
        s3bucket.getSignedUrl('putObject', params, function(err, url) {
            if (err) {
                reject(err);
            }
            resolve(url);
        });
    });
}

/**
 * Delete a object.
 * @param {string} filename path at object to delete
 * @returns {string || object} Return validation or a error object.
 */
exports.deleteObject = (filename) => {
    const params = {
        Bucket: bucket_name,
        Key: filename
    };

    // Uploading files to the bucket
    return new Promise ((resolve, reject) => {
        s3bucket.deleteObject(params, function(err, data) {
            if (err) {
                reject(err);
            }
            resolve(data);
        });
    });
}

/**
 * Crypt a text.
 * @param {string} text Text to crypt. 
 * @returns Text crypted.
 */
exports.encrypt = (text) => {
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
    return encrypted.toString('hex');
}

/**
 * Decrypt a text.
 * @param {string} text Text to decrypt. 
 * @returns Text decrypted.
 */
exports.decrypt = (text) => {
    const decipher = crypto.createDecipheriv(algorithm, secretKey, iv);
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(text, 'hex')), decipher.final()]);
    return decrpyted.toString();
}

/**
 * Mask an email.
 * @param {string} email Email to mask. 
 * @returns Email masked.
 */
exports.maskEmail = (email) => {
    const emailMask2Options = {
        maskWith: "*", 
        unmaskedStartCharactersBeforeAt: 10,
        unmaskedEndCharactersAfterAt: 0,
        maskAtTheRate: false
    };
    return MaskData.maskEmail2(email, emailMask2Options);
}