# OC7 Groupomania

Groupomania is project 7 of the OpenClassrooms web developer training.

Groupomania is a corporate social network.

Version test : https://sainormagoden.gitlab.io/FrancoisDarrigade_7_05052021

## Usage
---
This project use Nodejs, version 14.15.3, with Vuejs 3, for front-end, and express and sequelize, for back-end.

Use AWS for the SQL database (RDS) and for storing images (S3).

## Pre-Installation
---
### Database - AWS RDS
You need a database with RDS, a Amazon Web Service.

For create a free database, you can click [here](https://aws.amazon.com/premiumsupport/knowledge-center/free-tier-rds-launch/?nc1=h_ls).

I choose to accept 'Public accessibility' and to connect with 'Password authentication'.

### Image - AWS S3
You need a bucket with S3, a Amazon Web Service.

You can create easily a bucket, but I turn off 'Block public access'.

I create a bucket policy for accept just .png, .jpeg and .gif.
```json
{
    "Version": "xxxx-xx-xx", // Change version
    "Id": "Policy123456789", // Change ID
    "Statement": [
        {
            "Sid": "Stmt1464968483619",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<YourID>:root" // Add your ID
            },
            "Action": "s3:PutObject",
            "Resource": [
                "arn:aws:s3:::<YourBucket>/*.jpg",
                "arn:aws:s3:::<YourBucket>/*.jpeg",
                "arn:aws:s3:::<YourBucket>/*.png",
                "arn:aws:s3:::<YourBucket>/*.gif",
                "arn:aws:s3:::<YourBucket>/*.JPG",
                "arn:aws:s3:::<YourBucket>/*.JPEG",
                "arn:aws:s3:::<YourBucket>/*.PNG",
                "arn:aws:s3:::<YourBucket>/*.GIF"
            ]
        },
        {
            "Sid": "Stmt1464968483619",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:PutObject",
            "NotResource": [
                "arn:aws:s3:::<YourBucket>/*.jpg",
                "arn:aws:s3:::<YourBucket>/*.jpeg",
                "arn:aws:s3:::<YourBucket>/*.png",
                "arn:aws:s3:::<YourBucket>/*.gif",
                "arn:aws:s3:::<YourBucket>/*.JPG",
                "arn:aws:s3:::<YourBucket>/*.JPEG",
                "arn:aws:s3:::<YourBucket>/*.PNG",
                "arn:aws:s3:::<YourBucket>/*.GIF"
            ]
        }
    ]
}
```
And I add CORS
```json
[
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "PUT",
            "POST",
            "DELETE"
        ],
        "AllowedOrigins": [
            "*"
        ],
        "ExposeHeaders": [
            "x-amz-server-side-encryption",
            "x-amz-request-id",
            "x-amz-id-2"
        ],
        "MaxAgeSeconds": 3000
    }
]
```

## Installation
---
### For front-end :

Check in 'src/config/config.js' if API_URL is correct.

API_URL depending when you have your back-end, it's 'http(s)://YourUrlForBackEnd/api/'.

In your terminal :

```npm
npm install

# Just for dev
npm run serve

# For Prod
npm run build
mv public public-vue 
mv dist public
```
### For back-end :

Go in 'back_end' folder or install it on a servor.

Create a .env file in back_end folder, for it use 'back_end/exemple.env'.

'DB_' = AWS RDS

DB_INIT need to is 'true' for initialising database and to have default informations in website. After turn it in false.

'JWT_TOKEN', 'SESSION_NAME', 'SESSION_SECRET', 'CRYPTR_SECRET', 'CRYPTO_IV' can have a random long string.

AWS_ = AWS S3.

In your terminal :
```npm
npm install

npm nodemon server
```
